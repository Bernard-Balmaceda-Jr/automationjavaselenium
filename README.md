**PROJECT DETAILS:**
- Description: A simple CI/CD test automation workflow setup,
for testing a simple web application.
- Project: A simple web application (https://www.saucedemo.com)
- JIRA X-Ray: Project management JIRA plugin (https://sandbox.xpand-it.com/)
X-Ray Tutorials: https://www.youtube.com/watch?v=73a16-yXAfA
- GitLab: CI/CD repo automation (https://gitlab.com/Bernard-Balmaceda-Jr/automationjavaselenium/)
Package Management: Maven?
- Language: Java
- Framework: Selenium, JUnit
- IDE: Intellij


**GITLAB SETUP:**
- Setup Project
- Setup repo


**INTELLIJ SETUP:**
- Setup New Project (ref: https://www.youtube.com/watch?v=De2SF7wy3uE)
- Addon Selenium
- Addon WebDriver
- Addon Junit
