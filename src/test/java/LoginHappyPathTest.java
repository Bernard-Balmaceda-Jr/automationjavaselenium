import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginHappyPathTest extends TestCaseTemplate {

    @Test
    public void loginSuccessTest(){
        //  Description: LoginSuccess = valid username + valid password combo

        //  Preconditions

        //  Test Steps
        WebElement username=getWebDriver().findElement(By.id("user-name"));
        username.sendKeys("standard_user");
        WebElement password=getWebDriver().findElement(By.id("password"));
        password.sendKeys("secret_sauce");
        WebElement login=getWebDriver().findElement(By.id("login-button"));
        login.click();

        //  Dynamic Delay
        WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
        WebElement successfulLogin = wait.until(
            (
                ExpectedConditions.visibilityOfElementLocated(
                    By.className("product_label")
                )
            )
        );
        String actualMessage=successfulLogin.getText();
        String expectedMessage="Products";

        //  Verification Steps
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    //  TODO: Please create issue for TEST DATA
    //   (Framework dynamically obtain test data)
}
