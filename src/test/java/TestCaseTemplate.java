import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.concurrent.TimeUnit;

public class TestCaseTemplate {
    private WebDriver driver;

    public WebDriver getWebDriver(){
        return this.driver;
    }

    @Before
    public void setup(){
        //  Online bonigarcia WebDriver
        System.out.println("Test Setup");
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();
        opt.setHeadless(true);    //  Enable to run automation without blind
        opt.addArguments("--no-sandbox");
        opt.addArguments("--disable-dev-shm-usage");
        this.driver = new ChromeDriver(opt);
        this.driver.get("https://www.saucedemo.com/");
        this.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    //  TODO: Please do not remove - This is an alternate option to above
    //    @Before
    //    public void setup(){
    //        //  Local Chrome Webdriver
    //        String geckoDriver = "webdriver.chrome.driver";
    //        String geckoDriverLocation = "../../Framework/chromedriver";
    //        System.setProperty(geckoDriver, geckoDriverLocation);
    //        ChromeOptions opt = new ChromeOptions();
    //        driver = new ChromeDriver(opt);
    //        driver.get("https://www.saucedemo.com/");
    //    }

    //  TODO: Chrome driver Options
    //  TODO: Firefox driver Options

    @After
    public void closeBrowser(){
        System.out.println("Closing Browser");
        this.driver.quit();
    }
}
