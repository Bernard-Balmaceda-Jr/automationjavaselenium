import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class LoginAlternatePathTemplate extends TestCaseTemplate{

    @Test
    public void loginTemplate(
        String username_args,
        String password_args,
        String err_msg_args)
    {
        //  Preconditions:

        //  Test Steps
        WebElement username = getWebDriver().findElement(By.id("user-name"));
        username.sendKeys(username_args);
        getWebDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        WebElement password = getWebDriver().findElement(By.id("password"));
        password.sendKeys(password_args);
        getWebDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        WebElement login=getWebDriver().findElement(By.id("login-button"));
        login.click();
        getWebDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        //  Dynamic Delay
        WebElement divElement = getWebDriver().findElement(By.tagName("h3"));
        String actual_error_msg = divElement.getText();

        //  Verification Steps
        Assert.assertEquals(actual_error_msg, err_msg_args);
        getWebDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }
}
