import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class LoginAlternatePathTest {
    private String username_params;
    private String password_params;
    private String err_msg_params;
    private LoginAlternatePathTemplate loginAlternatePath;

    //  TODO: to be deleted - create issue
    //  TODO: ref above, the following strings needs to be held in test data framework
    private static String err_msg_invalid_combo = "Epic sadface: Username and password do not match any user in this service";
    private static String err_msg_blank_user = "Epic sadface: Username is required";
    private static String err_msg_blank_password = "Epic sadface: Password is required";
    private static String err_msg_locked_out = "Epic sadface: Sorry, this user has been locked out.";

    public LoginAlternatePathTest(
        String username_params,
        String password_params,
        String err_msg_params)
    {
        super();
        this.username_params = username_params;
        this.password_params = password_params;
        this.err_msg_params = err_msg_params;
    }

    @Before
    public void init(){
        loginAlternatePath = new LoginAlternatePathTemplate();
        loginAlternatePath.setup();
    }

    @Parameterized.Parameters
    public static Collection data(){
        return Arrays.asList(new Object[][]{
            //  TODO: Create a aux for test Data - Create Issue for this
            //  TODO: Investigate a possible matrix loop - Create Issue for this
            //  err_msg: "Epic sadface: Password is required"
            {"standard_user", "", err_msg_blank_password},
            {"locked_out_user", "", err_msg_blank_password},

            //  TODO: Issue memory limitation on runner

            {"problem_user", "", err_msg_blank_password},
            {"performance_glitch_user", "", err_msg_blank_password},

            //  err_msg: "Epic sadface: Username is required"
            {"", "secret_sauce", err_msg_blank_user},
            {"", "", err_msg_blank_user},

            //  err_msg: "Epic sadface: Username and password do not match any user in this service";
            {"invalid_user", "secret_sauce", err_msg_invalid_combo},
            {"standard_user", "invalid_password", err_msg_invalid_combo},
            {"problem_user", "invalid_password", err_msg_invalid_combo},
            {"performance_glitch_user", "invalid_password", err_msg_invalid_combo},
            {"invalid_user", "invalid_password", err_msg_invalid_combo},
            {"locked_out_user", "invalid_password", err_msg_invalid_combo},

            // err_msg: "Epic sadface: Sorry, this user has been locked out."
            {"locked_out_user", "secret_sauce", err_msg_locked_out},

            //  TODO: Issue memory limitation on runner
        });
    }

    @Test
    public void loginAlternatePathTest(){
        loginAlternatePath.loginTemplate(
            username_params,
            password_params,
            err_msg_params
        );
    }
}
