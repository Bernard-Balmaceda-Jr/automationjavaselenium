INTELLIJ SETUP

Project Setup
1. File>New>Project>
* Select Modules: Maven
* Select Project SDK: java version "15.0.2"
* Select Next
2. Fill New Project details:
* Name:TestAutomationJavaSelenium
* Location:'/home/user/Desktop/Research/TestAutomationJavaSelenium'
* Artifact Coordinates - GroupID: saucedemo.com
* Artifact Coordinates - ArtifactId: 
* Artifact Coordinates - Version: 1.0-SNAPSHOT


Intellij Project junit Modules Setup
(Ref article: https://stackoverflow.com/questions/19330832/setting-up-junit-with-intellij-idea)
1. Right-click Project>TestAutomationJavaSelenium>Open Module Settings
2. Select New Project Library + button in Project Settings>Libraries>+
3. Select From Maven
4. From Project Structure, select New Project Library (+ button) 
from ProjectSettings>Libraries>+>From Maven
5. From Download Library from Maven Repository, enter 'junit:junit:4.13' 
and tick the Download to your Location


Intellij Project Module Selenium Setup
(Ref article: https://www.youtube.com/watch?v=De2SF7wy3uE)
1. Download https://selenium-release.storage.googleapis.com/3.141/selenium-java-3.141.59.zip
2. Unzip in 'jar_location': /home/junior/Desktop/Reseach/Framework
3. File>ProjectStructure>Dependencies>+ button>JARs or Directories
4. Select all jar files including those in the lib folder from 'jar_location'.
5. Right click Project Folder>Maven>Reload projects
